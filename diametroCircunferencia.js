function diametroCircunferencia(diametro) {
    const pi = 3.1416;
    var diametro;
    let area= (pi*(diametro ** 2))/4;
    return area;
}
let diametro= 40;
let area= diametroCircunferencia(diametro);
console.log('El area de la circunferencia es: ', area);