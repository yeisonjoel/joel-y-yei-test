// Definir una funcion para calcular el factorial de un numero dado.

function Factorial(n) {
    if(n == 0) {
        return 1;
    }else {
        return n*Factorial(n-1);
    }
}
console.log(Factorial(4)); // 24